package main

import (
	"fmt"
	"http"
	"io/ioutil"
	"json"
	"url"
	"os"
	"strings"
	"exec"
)

type Catch struct {
	user ReplyUser
	accessToken string
}

type Note struct {
	text string
}

func (c *Catch) SignIn(user, pass string) {
	req, err := http.NewRequest("POST", "https://api.catch.com/v2/user.json", nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	req.SetBasicAuth(user, pass)

	http := http.Client{}
	resp, err := http.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	data, err := ioutil.ReadAll(resp.Body)
	json.Unmarshal(data, &c.user)
	resp.Body.Close()
}

func (c *Catch) AccessToken() string {
	return c.user.User.Access_token
}

func (c *Catch) EncodedAccessToken() string {
	return url.QueryEscape(c.user.User.Access_token)
}

func (c *Catch) AddNote(note *Note) {

	values := url.Values{}
	values.Add("text", note.text)

	url := "https://api.catch.com/v2/notes.json?access_token=" + c.EncodedAccessToken()
	http := http.Client{}
	resp, err := http.PostForm(url, values)
	if err != nil {
		fmt.Println(err)
	}
	resp.Body.Close()

}

type ReplyUser struct {
	ServerTime string
	User ReplyUser_User
}

type ReplyUser_User struct {
	Id int32
	User_name string
	Email string
	Access_token string
}

var editor string
var tmpdir string
var user string
var pass string

func init() {
	editor = "vi"
}

func parseEnviron() {
	editor = os.Getenv("EDITOR")
	if editor == "" {
		editor = "vi"
	}

	tmpdir = os.Getenv("CATCHTMP")
	if tmpdir == "" {
		tmpdir = os.ShellExpand("$HOME/.catch")
	}
}

func loadUserPass() {
	data, err := ioutil.ReadFile(os.ShellExpand("$HOME/.catchpass"))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	arr := strings.Split(string(data), "\n")
	if len(arr) < 2 {
		fmt.Println("Error reading .catchpass")
		os.Exit(1)
	}
	user = arr[0]
	pass = arr[1]
}

func main() {

	parseEnviron()
	loadUserPass()

	if tmpdir == "" {
		fmt.Printf("Please set $CATCHTMP to store a temporary file\n")
		os.Exit(1)
	}


	_, err := os.Stat(tmpdir)
	if err != nil {
		err = os.MkdirAll(tmpdir, 0700)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	file, err := ioutil.TempFile(tmpdir, "catchtmp")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer os.Remove(file.Name())
	file.Close()

	cmd := exec.Command(editor, file.Name())
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Env = os.Environ()

	if err != nil {
		fmt.Println(err)
		return
	}
	err = cmd.Run()
	if err != nil {
		fmt.Println(err)
		return
	}

	text, err := ioutil.ReadFile(file.Name())
	if err != nil {
		fmt.Println(err)
		return
	}

	if strings.TrimSpace(string(text)) == "" {
		fmt.Println("Text is empty. Won't upload it")
		return
	}

	c := Catch{}
	c.SignIn(user, pass)
	//fmt.Println(c.AccessToken())

	note := Note{}
	note.text = string(text)
	c.AddNote(&note)

	fmt.Println("Uploaded")
}
