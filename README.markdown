catch-golang
===============

Command line client to make and upload a note to catch.com


How to use
============

Write your username and password of catch.com in $HOME/.catchpass

    $ cat ~/.catchpass
    myname
    mypass

That's all. Just run `catch` and it will launch text editor. If you write some text in the file and exit the editor, `catch` will upload the text to catch.com.



